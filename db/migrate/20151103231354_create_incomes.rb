class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.references :category, index: true, foreign_key: true
      t.string :name
      t.integer :amount
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
