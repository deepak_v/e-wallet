class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # store last url - this is needed for post-login redirect to whatever the user last visited. return unless request.get?
  before_action :authenticate_user!

  def after_sign_out_path_for(resource)
  	request.referrer
  end
end