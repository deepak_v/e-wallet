class Income < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  validates :category_id, :presence => true
  validates :name, :presence => true
  validates :amount, :presence => true, :numericality => { :greater_than => 0 }
end
