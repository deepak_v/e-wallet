class Category < ActiveRecord::Base
	belongs_to :user
	has_many :costs, dependent: :destroy
	has_many :incomes, dependent: :destroy
	validates :name, :presence => true
	validates_uniqueness_of :name
end
