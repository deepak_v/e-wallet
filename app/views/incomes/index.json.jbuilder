json.array!(@incomes) do |income|
  json.extract! income, :id, :category_id, :name, :amount, :user_id
  json.url income_url(income, format: :json)
end
