json.array!(@costs) do |cost|
  json.extract! cost, :id, :category_id, :name, :amount, :user_id
  json.url cost_url(cost, format: :json)
end
